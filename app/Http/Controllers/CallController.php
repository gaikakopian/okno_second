<?php

namespace App\Http\Controllers;

use App\Integrations\Neo;
use App\Models\Call;
use App\Models\City;
use App\Models\Setting;
use App\Models\Skorozvon;
use Illuminate\Http\Request;

class CallController extends Controller
{
    public function changeStatus(Request $request){
        $setting = Setting::where('title', 'send')->first();

        if(!empty($request->inn) && !empty($request->status) && !empty($setting) && !empty($setting->value)){
            $call = Call::where('inn', $request->inn)->first();
            if($call){
                $body = [];
                $body['id'] = $call->ed_id;
                $body['status'] = $request->status;

                $neo = new Neo();
                try{
                    $res = $neo->patchKcc($body);
                    $call->status = $body['status'];
                    $call->save();
                } catch (\Exception $e){
                }
            }
        }
        return response()->json(['success'=>'true'] , 200);
    }
}
