<?php

namespace App\Http\Controllers;

use App\Integrations\Neo;
use App\Models\City;
use App\Models\MerchantBranch;
use App\Models\Region;
use App\Models\Setting;
use App\Models\Skorozvon;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $setting = Setting::where('title', 'send')->first();
        return view('pages.dashboard', compact('setting'));
    }

    public function turnOn()
    {
        $setting = Setting::where('title', 'send')->first();
        $setting->value = 1;
        $setting->save();

        return redirect('/');
    }

    public function turnOff()
    {
        $setting = Setting::where('title', 'send')->first();
        $setting->value = 0;
        $setting->save();

        return redirect('/');
    }

    public function test()
    {

    }
}
