<?php

namespace App\Http\Controllers;

use App\Integrations\Neo;
use App\Models\Call;
use App\Models\DataFile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Exports\ExcelImport;
use DateTime;

class DataFilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $files = DataFile::with('contents')->orderBy('created_at', 'desc')->paginate(50);
        $now = new DateTime();
        $allowed = [];

        foreach ($files as $file){
            $updated = new DateTime($file->last_updated);
            $interval = $now->diff($updated);
            $minutes = $interval->days * 24 * 60;
            $minutes += $interval->h * 60;
            $minutes += $interval->i;

            if($minutes > 10){
                $allowed[$file->id] = true;
            }else{
                $allowed[$file->id] = false;
            }
        }
        return view('pages.files.index', compact('files','allowed'));
    }

    public function edit(Request $request)
    {
        $file = DataFile::find($request->id);
        return view('pages.files.edit', compact('file'));
    }

    public function update(Request $request){
        $file = DataFile::find($request->id);
        if($file->times_repeated + $request->times_to_repeat < 6){
            $file->times_to_repeat = $request->times_to_repeat;
            $file->save();
        }
        return redirect('/files');
    }

    public function save(Request $request){
        $file = DataFile::find($request->id);
        $data = \GuzzleHttp\json_decode($file->last_results, true);

        return \Maatwebsite\Excel\Facades\Excel::download(new ExcelImport($data), 'export.xlsx');
    }

    public function send(Request $request){
        $file = DataFile::with('contents')->find($request->id);
        if($file){
            if(($file->times_repeated + $file->times_to_repeat) < 5){
                $inns = [];
                $last_updated = new DateTime();

                foreach ($file->contents as $content){
                    $neo = new Neo();
                    try{
                        $body = [];
                        $body['inn'] = $content->inn;
                        $body['phone'] = $content->phone;
                        $res = $neo->queryKcc($body);

                        if($res->status == 'allowed'){
                            array_push($inns, ['inn' => $body['inn'], 'phone' => $body['phone'], 'status' => 'Разрешен']);

                            $call = Call::where('inn', $body['inn'])->first();
                            if($call){
                                $call->delete();
                            }
                            $new_call = new Call();
                            $new_call->inn = $body['inn'];
                            $new_call->ed_id = $res->id;
                            $new_call->status = 'allowed';
                            $new_call->save();

                            $content->last_updated = new DateTime();
                            $content->save();
                            $last_updated = new DateTime();
                        }
                    } catch (\Exception $e){
                    }
                }
                $file->times_repeated = $file->times_repeated + 1;
                $file->last_results = json_encode($inns);
                $file->last_updated = $last_updated;
                $file->save();
            }
        }
        return redirect('/files');

        $files = DataFile::with('contents')->orderBy('created_at', 'desc')->paginate(50);
        $now = new DateTime();
        $allowed = [];

        foreach ($files as $file){
            $updated = new DateTime($file->last_updated);
            $interval = $now->diff($updated);
            $minutes = $interval->days * 24 * 60;
            $minutes += $interval->h * 60;
            $minutes += $interval->i;

            if($minutes > 10){
                $allowed[$file->id] = true;
            }else{
                $allowed[$file->id] = false;
            }
        }
        return view('pages.files.index', compact('files','allowed'));
    }

    public function delete(Request $request){
        $file = DataFile::with('contents')->find($request->id);
        if($file){
            foreach ($file->contents as $content) {
                $call = Call::where('inn', $content->inn)->first();
                if ($call) {
                    $call->delete();
                }
                $content->delete();
            }
        }
        $file->delete();

        return redirect('/files');
    }
}
