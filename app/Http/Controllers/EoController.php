<?php

namespace App\Http\Controllers;

use App\Integrations\Neo;
use App\Models\Call;
use App\Models\DataContent;
use App\Models\DataFile;
use Illuminate\Http\Request;
use DateTime;


class EoController extends Controller
{
    public function storeData(Request $request){
        ini_set('max_execution_time', 6000);

        $file = new DataFile();
        $file->name = $request->file_name;
        $file->last_updated = new DateTime();
        $file->times_repeated = 0;
        $file->times_to_repeat = 0;
        $file->last_results = null;
        $file->save();

        $get_data = \GuzzleHttp\json_decode($request->json, true);
        $inns = [];
        $last_updated = new DateTime();

        foreach ($get_data as $content){
            $new_content = new DataContent();
            $new_content->file_id = $file->id;
            $new_content->inn = $content['inn'];
            $new_content->phone = $content['phone'];
            $new_content->last_updated = new DateTime();
            $new_content->save();

            $neo = new Neo();
            try{
                $body = [];
                $body['inn'] = $content['inn'];
                $body['phone'] = $content['phone'];
                $res = $neo->queryKcc($body);

                if($res->status == 'allowed'){
                    array_push($inns, ['inn' => $body['inn'], 'phone' => $body['phone'], 'status' => 'Разрешен']);

                    $call = Call::where('inn', $body['inn'])->first();
                    if($call){
                        $call->delete();
                    }
                    $new_call = new Call();
                    $new_call->inn = $body['inn'];
                    $new_call->ed_id = $res->id;
                    $new_call->status = 'allowed';
                    $new_call->save();

                    $new_content->last_updated = new DateTime();
                    $new_content->save();
                    $last_updated = new DateTime();
                }

                if($res->status == 'blocked'){
                    array_push($inns, ['inn' => $body['inn'], 'phone' => $body['phone'], 'status' => 'Заблокирован другим']);
                }
                if($res->status == 'forbidden'){
                    array_push($inns, ['inn' => $body['inn'], 'phone' => $body['phone'], 'status' => 'Не звонить']);
                }
            } catch (\Exception $e){
            }
        }

        $file->times_repeated = 1;
        $file->times_to_repeat = 2;
        $file->last_results = json_encode($inns);
        $file->last_updated = $last_updated;
        $file->save();

        return response()->json(['success'=>'true'] , 200);
    }
}
