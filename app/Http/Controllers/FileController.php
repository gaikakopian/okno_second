<?php

namespace App\Http\Controllers;

use App\Exports\ExcelImport;
use App\Exports\ExcelUpload;
use App\Integrations\Neo;
use App\Models\Call;
use App\Models\File;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;

class FileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function check(Request $request)
    {
        ini_set('max_execution_time', 600);
        if(empty($request->file)){
            return view('pages.dashboard');
        }

        $request->validate([
            'file' => 'required'
        ]);

        $data = \Maatwebsite\Excel\Facades\Excel::toArray(new ExcelUpload(), $request->file);
        $all_count = 0;
        $free = 0;
        $double = 0;

        $inns = [];

        if(count($data)){
            foreach ($data[0] as $key => $value) {
                if($value[0]){
                    $string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $value[0]);
                    if(!empty($string)){
                        if(strlen ( $string ) == 9 || strlen ( $string ) == 11 ){
                            $string = '0'.$string;
                        }
                        $neo = new Neo();
                        try{
                            $res = $neo->query($string);
                            $free += 1;
                            array_push($inns, ['inn' => $string, 'status' => 'Свободен']);
                        } catch (\Exception $e){
                            $double += 1;
                            array_push($inns, ['inn' => $string, 'status' => 'Занят']);
                        }
                        $all_count += 1;
                    }
                }
            }
        }

        $response['counts']['all_count'] = $all_count;
        $response['counts']['free'] = $free;
        $response['counts']['double'] = $double;

        $file = new File();
        $file->json = json_encode($inns);
        $file->save();

        $response['inn_id'] = $file->id;

        return view('pages.dashboard', compact('response'));

    }

    public function checkKcc(Request $request)
    {
        ini_set('max_execution_time', 600);
        if(empty($request->file)){
            return view('pages.dashboard');
        }

        $request->validate([
            'file' => 'required'
        ]);

        $data = \Maatwebsite\Excel\Facades\Excel::toArray(new ExcelUpload(), $request->file);

        $all_count = 0;
        $free = 0;
        $double = 0;
        $error = 0;

        $inns = [];

        if(count($data)){
            foreach ($data[0] as $key => $value) {
                if($value[0] && $value[1]){
                    $phone = preg_replace('/[^\p{L}\p{N}\s]/u', '', $value[1]);
                    $check = substr($phone, 0, 1);
                    if($check == '8'){
                        $phone = '7'.substr($phone, 1, strlen($phone));
                    }
                    if($check != '+'){
                        $phone = '+'.$phone;
                    }


                    $body = [];
                    $body['inn'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', $value[0]);
                    $body['phone'] = $phone;

                    $neo = new Neo();
                    try{
                        $res = $neo->queryKcc($body);
                        if($res->status == 'allowed'){
                            array_push($inns, ['inn' => $body['inn'], 'phone' => $body['phone'], 'status' => 'Разрешен']);
                            $free += 1;

                            $call = Call::where('inn', $body['inn'])->first();
                            if($call){
                                $call->delete();
                            }
                            $new_call = new Call();
                            $new_call->inn = $body['inn'];
                            $new_call->ed_id = $res->id;
                            $new_call->status = 'allowed';
                            $new_call->save();
                        }else{
                            $double += 1;
                        }
                    } catch (\Exception $e){
                        $error += 1;
                        //array_push($inns, ['inn' => $string, 'status' => 'Занят']);
                    }
                    $all_count += 1;

                }

//                if($value[0]){
//                    $string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $value[0]);
//                    if(!empty($string)){
//                        if(strlen ( $string ) == 9 || strlen ( $string ) == 11 ){
//                            $string = '0'.$string;
//                        }
//                        $neo = new Neo();
//                        try{
//                            $res = $neo->query($string);
//                            $free += 1;
//                            array_push($inns, ['inn' => $string, 'status' => 'Свободен']);
//                        } catch (\Exception $e){
//                            $double += 1;
//                            array_push($inns, ['inn' => $string, 'status' => 'Занят']);
//                        }
//                        $all_count += 1;
//                    }
//                }
            }
        }

        $response['counts']['all_count'] = $all_count;
        $response['counts']['free'] = $free;
        $response['counts']['double'] = $double;
        $response['counts']['error'] = $error;

        $file = new File();
        $file->json = json_encode($inns);
        $file->save();

        $response['inn_id'] = $file->id;

        $setting = Setting::where('title', 'send')->first();

        return view('pages.dashboard', compact('response', 'setting'));

    }

    public function download(Request $request){
        $file = File::find($request->id);
        $data = \GuzzleHttp\json_decode($file->json, true);

        return \Maatwebsite\Excel\Facades\Excel::download(new ExcelImport($data), 'export.xlsx');

    }
}
