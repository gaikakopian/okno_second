<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class DataFile extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_updated',
        'times_repeated',
        'times_to_repeat',
        'last_results'
    ];

    public function contents()
    {
        return $this->hasMany(DataContent::class , 'file_id','id');
    }
}
