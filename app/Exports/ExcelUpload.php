<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExcelUpload implements FromCollection, WithHeadings
{
    use Exportable;


    public function collection()
    {
        return collect();
    }

    public function headings(): array
    {
        return [
            'Инн',
            'Статус'
        ];
    }

}