<?php

namespace App\Console\Commands;

use App\Models\DataFile;
use Illuminate\Console\Command;
use App\Integrations\Neo;
use App\Models\Call;
use DateTime;

class CheckFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $files = DataFile::with('contents')->where('times_to_repeat','>',0)->get();
        $now = new DateTime();
        foreach ($files as $file){
            $updated = new DateTime($file->last_updated);
            $interval = $now->diff($updated);
            $minutes = $interval->days * 24 * 60;
            $minutes += $interval->h * 60;
            $minutes += $interval->i;

            if(($file->times_repeated + $file->times_to_repeat) < 5 && $minutes > 10){
                $inns = [];
                $last_updated = new DateTime();

                foreach ($file->contents as $content){
                    $neo = new Neo();
                    try{
                        $body = [];
                        $body['inn'] = $content->inn;
                        $body['phone'] = $content->phone;
                        $res = $neo->queryKcc($body);

                        if($res->status == 'allowed'){
                            array_push($inns, ['inn' => $body['inn'], 'phone' => $body['phone'], 'status' => 'Разрешен']);

                            $call = Call::where('inn', $body['inn'])->first();
                            if($call){
                                $call->delete();
                            }
                            $new_call = new Call();
                            $new_call->inn = $body['inn'];
                            $new_call->ed_id = $res->id;
                            $new_call->status = 'allowed';
                            $new_call->save();

                            $content->last_updated = new DateTime();
                            $content->save();
                            $last_updated = new DateTime();
                        }
                    } catch (\Exception $e){
                    }
                }
                $file->times_repeated = $file->times_repeated + 1;
                $file->times_to_repeat = $file->times_to_repeat - 1;
                $file->last_results = json_encode($inns);
                $file->last_updated = $last_updated;
                $file->save();
            }
        }
    }
}
