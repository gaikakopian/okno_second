<?php
namespace App\Integrations;

use GuzzleHttp\Client;

class Neo{

    private $url = 'https://ppapi.dev.dasreda.ru/api/v1/';
    private $token = 'e3c94f7fcc25e0720b56baa933c0b8b2';

    public function __construct()
    {
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    private function request($url , $body = [])
    {
        $client = new Client([
            'headers' => [
                'Authorization' => 'Token ' . $this->token,
            ]
        ]);

        try {
            $response = $client->get($this->url . $url );
        } catch (Exception $e){
            return $e->getMessage();
        }

        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    private function requestPost($url , $body = [])
    {
        $client = new Client([
            'headers' => [
                'Authorization' => 'Token ' . $this->token,
                'Content-Type' => 'application/json'
            ]
        ]);

        try {
//        $client->setContentType('application/x-www-form-urlencoded');
            $response = $client->post($this->url . $url , [
                'json' => $body
            ]);
        } catch (Exception $e){
            return $e->getMessage();
        }
        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    private function requestPatch($url , $body = [])
    {
        $client = new Client([
            'headers' => [
                'Authorization' => 'Token ' . $this->token,
                'Content-Type' => 'application/json'
            ]
        ]);

        try {
//        $client->setContentType('application/x-www-form-urlencoded');
            $response = $client->patch($this->url . $url , [
                'json' => $body
            ]);
        } catch (Exception $e){
            return $e->getMessage();
        }
        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    /**
     * @param $uid
     * @return mixed|string
     */
    public function query($inn){
        try {

            $body['data'] = [];
            $body['data']['inn'] = $inn;
            $body['data']['accept_blocked'] = 1;
            $body['data']['product_ids'][] = '1';

            $request = $this->requestPost('sber_mq/order/build', $body);
        } catch (Exception $e){
            return $e->getMessage();
        }

        return $request;
    }

    public function queryKcc($body){
        try {
            $sentBody = [];
            $sentBody['data'] = $body;
            $request = $this->requestPost('call_easy/call_requests', $sentBody);
        } catch (Exception $e){
            return $e->getMessage();
        }

        return $request;
    }

    public function patchKcc($body){
        try {
            $sentBody = [];
            $sentBody['data'] = [];
            $sentBody['data']['call_status'] = $body['status'];

            $request = $this->requestPatch('call_easy/call_requests/'.$body['id'], $sentBody);
        } catch (Exception $e){
            return $e->getMessage();
        }

        return $request;
    }

    public function queryDataArray($array){
        try {

            $body['data'] = $array;
            $body['data']['accept_blocked'] = 1;
            $body['data']['product_ids'][] = '1';

            $request = $this->requestPost('sber_mq/order', $body);
        } catch (Exception $e){
            return $e->getMessage();
        }

        return $request;
    }

    public function getMerchants($page){
        try {
            $request = $this->request('sber_mq/merchant_branch?per_page=100&page='.$page);
        } catch (Exception $e){
            return $e->getMessage();
        }

        return $request;
    }

    public function getCities($page){
        try {
            $request = $this->request('sber_mq/city?per_page=100&page='.$page);
        } catch (Exception $e){
            return $e->getMessage();
        }

        return $request;
    }

    public function getStatuses($page){
        try {
            $request = $this->request('api/v1/call_easy/call_requests/statuses');
        } catch (Exception $e){
            return $e->getMessage();
        }

        return $request;
    }
}