<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Route::get('/skorozvon',  'SkorozvonController@handleAnketa');
//Route::post('/skorozvon',  'SkorozvonController@handleAnketa');

Route::get('/', 'DashboardController@index')->name('home');

Route::get('/turn-on', 'DashboardController@turnOn')->name('turn-on');
Route::get('/turn-off', 'DashboardController@turnOff')->name('turn-off');

Route::get('/home', 'DashboardController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/users/create', 'UserController@create')->name('users-create');
Route::get('/users/edit/{id}', 'UserController@edit')->name('users-edit');

Route::get('/files', 'DataFilesController@index')->name('files');
Route::get('/files/save/{id}', 'DataFilesController@save')->name('files');
Route::get('/files/send/{id}', 'DataFilesController@send')->name('files');
Route::get('/files/edit/{id}', 'DataFilesController@edit')->name('files-edit');
Route::get('/files/delete/{id}', 'DataFilesController@delete')->name('files-delete');
Route::post('/files/update', 'DataFilesController@update')->name('files-update');

Route::get('/test', 'DashboardController@test')->name('test');

Route::post('/users/store', 'UserController@store')->name('users-store');
Route::get('/users/delete/{id}', 'UserController@delete')->name('users-delete');
Route::post('/users/update', 'UserController@update')->name('users-update');
Route::post('/files/check', 'FileController@checkKcc')->name('file-check');
Route::get('/files/download/{id}', 'FileController@download')->name('file-download');
