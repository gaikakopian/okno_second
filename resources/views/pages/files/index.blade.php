@extends('layouts.main')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Главная</a>
        </li>
        <li class="breadcrumb-item active">Файлы</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Файлы</div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Название</th>
                        <th>Дата Создания</th>
                        <th>Последняя отправка</th>
                        <th>Кол-во отправок</th>
                        <th>Кол-во Крон Отправок</th>
                        <th>Общее Кол-во ИНН</th>
                        <th>Кол-во разрешенных ИНН (последняя отправка)</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($files as $file)
                    <tr>
                        <td>{{ $file->id }}</td>
                        <td>{{ $file->name }}</td>
                        <td>{{ $file->created_at }}</td>
                        <td>{{ $file->last_updated }}</td>
                        <td>{{ $file->times_repeated }}</td>
                        <td>{{ $file->times_to_repeat }}</td>
                        <td>{{ count($file->contents) }}</td>
                        <td>{{ count(json_decode($file->last_results, true)) }}</td>
                        <td>
                            <a target="_blank" style="cursor:pointer" href="/files/save/{{ $file->id  }}" class="fas fa-save"></a>
                            <?php if(($file->times_repeated + $file->times_to_repeat) < 5 && $allowed[$file->id]){ ?>
                                <a style="cursor:pointer" href="/files/send/{{ $file->id  }}" class="fa fa-paper-plane"></a>
                            <?php } ?>
                            <a style="cursor:pointer" href="/files/edit/{{ $file->id  }}" class="fa fa-edit"></a>
                            <a style="cursor:pointer" href="/files/delete/{{ $file->id  }}" class="fas fa-trash"></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $files->links() }}
            </div>
        </div>
    </div>
@endsection
