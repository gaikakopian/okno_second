@extends('layouts.main')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Главная</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/files">Файлы</a>
        </li>
        <li class="breadcrumb-item active">Редактировать файл</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Редактировать файл</div>
        <div class="card-body">
            <form action="{{ route('files-update') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $file->id }}">
                <div class="form-group">
                    <div class="form-label-group">
                        <input value="{{ $file->times_to_repeat }}" name="times_to_repeat" type="number" id="firstName" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                        <label for="firstName">Колличество повторов</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Редактировать</button>
            </form>
        </div>
    </div>
@endsection