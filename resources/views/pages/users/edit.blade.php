@extends('layouts.main')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Главная</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/users">Пользователи</a>
        </li>
        <li class="breadcrumb-item active">Редактировать пользователя</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Редактировать пользователя</div>
        <div class="card-body">
            <form action="{{ route('users-update') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $user->id }}">
                <div class="form-group">
                    <div class="form-label-group">
                        <input value="{{ $user->name }}" name="name" type="text" id="firstName" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                        <label for="firstName">Имя</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input value="{{ $user->email }}" name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required">
                        <label for="inputEmail">Почта</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password">
                        <label for="inputPassword">Пароль</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Редактировать</button>
            </form>
        </div>
    </div>
@endsection