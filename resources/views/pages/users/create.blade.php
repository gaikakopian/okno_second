@extends('layouts.main')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Главная</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/users">Пользователи</a>
        </li>
        <li class="breadcrumb-item active">Создать Пользователя</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-table"></i>
        Создать Пользователя</div>
        <div class="card-body">
            <form action="{{ route('users-store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <div class="form-label-group">
                        <input name="name" type="text" id="firstName" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                        <label for="firstName">Имя</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required">
                        <label for="inputEmail">Почта</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required="required">
                        <label for="inputPassword">Пароль</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Создать</button>
            </form>
        </div>
    </div>
@endsection