@extends('layouts.main')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Главная</a>
        </li>
    </ol>
    <!-- DataTables Example -->
    <div class="card mb-3">
        <?php if(!empty($setting)){ ?>
            <div class="card-header">
                <?php if(!empty($setting->value)){ ?>
                    <a href="/turn-off" class="btn btn-danger btn-block">Выключить отправку</a>
                <?php }else{ ?>
                    <a href="/turn-on" class="btn btn-success btn-block">Включить отправку</a>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="card-header">
            <i class="fas fa-table"></i>
            Загрузите файл для проверки</div>

        <div class="card-body">
            <div>
                <form enctype="multipart/form-data" action="{{ route('file-check') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <input name="file" type="file" id="firstName" class="form-control">
                            <label for="firstName">Загрузите файл</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Проверить</button>
                </form>
                <?php if(!empty($response)){ ?>
                    <div class="card-body">
                    <div class="title" style="text-align: center; font-size: 20px; margin-bottom: 15px;">Результаты</div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="20%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Найдено</th>
                                    <th>Разрешено</th>
                                    <th>Не разрешено</th>
                                    <th>Инн находится в процессе</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $response['counts']['all_count'] }}</td>
                                    <td>{{ $response['counts']['free'] }}</td>
                                    <td>{{ $response['counts']['double'] }}</td>
                                    <td>{{ $response['counts']['error'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <div class="add-block pull-right" style="margin-bottom: 20px">
                            <a target="_blank" href="/files/download/{{ $response['inn_id'] }}" class="btn btn-primary btn-block">Скачать результаты</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <script>
        function downloadExcel(){
            alert('dsadas');
        }
    </script>
@endsection
