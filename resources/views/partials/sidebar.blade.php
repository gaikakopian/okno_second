<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Главная</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/files">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Файлы</span>
        </a>
    </li>
    <?php if(\Illuminate\Support\Facades\Auth::user()->role_name != 'user'){ ?>
        <li class="nav-item">
            <a class="nav-link" href="/users">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Пользователи</span>
            </a>
        </li>
    <?php } ?>
</ul>