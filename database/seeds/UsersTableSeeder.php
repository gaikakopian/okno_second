<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'  =>  'Admin',
                'email' =>  'admin@admin.com',
                'password'  =>  bcrypt('password'),
                'role_name' => 'admin'
            ]
        ];

        DB::table('users')->insert($data);
    }
}
