<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title'  =>  'send',
                'value' =>  1
            ]
        ];

        DB::table('settings')->insert($data);
    }
}
